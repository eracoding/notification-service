from django.utils.timezone import now
from rest_framework.test import APITestCase

from core.models import Mailing, Client, Message

# It is better to use baker to generate tests, but for the purpose of time I done this with simple test
# usage of coverage is important (I omitted)


class TestModel(APITestCase):
    def test_creates_mailings(self):
        mailing = Mailing.objects.create(date_start=now(),
                                         date_end=now(),
                                         text='Sample text',
                                         time_start=now().time(),
                                         time_end=now().time(),
                                         tag='sample',)
        self.assertIsInstance(mailing, Mailing)
        self.assertEqual(mailing.tag, 'sample')

    def test_creates_clients(self):
        client = Client.objects.create(phone_number='78983456789',
                                       mobile_operator_code='898',
                                       tag='sample',
                                       timezone='UTC')
        self.assertIsInstance(client, Client)
        self.assertEqual(client.phone_number, '78983456789')

    def test_creates_messages(self):
        mailing = Mailing.objects.create(date_start=now(),
                                         date_end=now(),
                                         text='Sample text',
                                         time_start=now().time(),
                                         time_end=now().time(),
                                         tag='sample',)
        client = Client.objects.create(phone_number='78983456789',
                                       mobile_operator_code='898',
                                       tag='sample',
                                       timezone='UTC')
        message = Message.objects.create(sending_status='Not send', client_id=client, mailing_id=mailing)
        self.assertIsInstance(message, Message)
        self.assertEqual(message.sending_status, 'Not send')
