import pytz
from django.db import models
from django.core.validators import RegexValidator
from django_prometheus.models import ExportModelOperationsMixin


class Client(ExportModelOperationsMixin('client'), models.Model):
    TIMEZONE = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_regex = RegexValidator(regex=r'^7\d{10}$', message="Format 7XXXXXXXXXX (X - number from 0 to 9)")
    phone_number = models.CharField(verbose_name='Phone Number', validators=[phone_regex], unique=True, max_length=11)
    mobile_operator_code = models.CharField(verbose_name='Operator Code', max_length=3, editable=False)
    tag = models.CharField(verbose_name='Search tags', max_length=100, blank=True)
    timezone = models.CharField(verbose_name='Time zone', max_length=32, choices=TIMEZONE, default='UTC')

    def __unicode__(self):
        return u'Client: %s, %s' % (self.id, self.phone_number)

    def __str__(self):
        return self.__unicode__()

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'

    def save(self, *args, **kwargs):
        self.mobile_operator_code = str(self.phone_number)[1:4]
        return super(Client, self).save(*args, **kwargs)
