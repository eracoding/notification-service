from django.db import models
from django.utils import timezone
from django_prometheus.models import ExportModelOperationsMixin


class Mailing(ExportModelOperationsMixin('mailing'), models.Model):
    date_start = models.DateTimeField(verbose_name='Start Date')
    time_start = models.TimeField(verbose_name='Start Time')
    text = models.TextField(max_length=255, verbose_name='Message')
    tag = models.CharField(max_length=100, verbose_name='Tags', blank=True)
    mobile_operator_code = models.CharField(verbose_name='Operator Code', max_length=3, blank=True)
    date_end = models.DateTimeField(verbose_name='End Date')
    time_end = models.TimeField(verbose_name='End Time')

    def __unicode__(self):
        return u'Mailing: %s, %s, %s, %s' % (self.date_start, self.time_start, self.text, self.tag
                                             + ' ' + self.mobile_operator_code)

    def __str__(self):
        return self.__unicode__()

    class Meta:
        verbose_name = 'Mailing'
        verbose_name_plural = 'Mailings'

    @property
    def is_send(self):
        now = timezone.now()
        if self.date_start <= now <= self.date_end:
            return True
        else:
            return False
