from django.db import models
from .Mailing import Mailing
from .Client import Client
from django_prometheus.models import ExportModelOperationsMixin


class Message(ExportModelOperationsMixin('message'), models.Model):
    SENT, PENDING = "Sent", "Not Send"

    STATUS_CHOICES = [
        (SENT, "Sent"),
        (PENDING, "Not Send"),
    ]

    time_create = models.DateTimeField(verbose_name='Time create', auto_now_add=True)
    sending_status = models.CharField(verbose_name='Sending status', max_length=15, choices=STATUS_CHOICES)
    mailing_id = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='mailing_id')
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='client_id')

    def __unicode__(self):
        return u'Message: %s, %s' % (self.mailing_id, self.client_id)

    def __str__(self):
        return self.__unicode__()

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'
