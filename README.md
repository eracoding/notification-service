# Сервис уведомлений

Проект разработан на основе DRF (Django Rest Framework) с использованием celery+redis (Cron) (мониторингом на flower) + инструментом swagger
---
## Для запуска проекта следуйте следующим действиям:
1. Склонируйте репозиторий:
```
git clone https://gitlab.com/eracoding/notification-service.git
```
2. Создайте виртуальную среду в папке проекта:
```
python -m venv .venv
```
3. Активировать среду Windows:
```
.venv\Scripts\activate
```
or Linux:
```
source .venv/bin/activate
```
4. Установите зависимости:
```
pip install -r requirements.txt
```
5. Сделайте и примените миграцию:
```
python manage.py makemigrations
python manage.py migrate
```
6. Запустите сервер:
```
python manage.py runserver
```
7. Запустите celery:
```
celery -A notification_service worker -l INFO
```
8. Запустите flower:
```
flower -A notification_service --port=5555
```
---
## Endpoints:
http://0.0.0.0:8000/docs/ - swagger docs of projcet

http://0.0.0.0:5555 - flower monitoring celery workers

http://0.0.0.0:8000/api/ - api of project

http://0.0.0.0:8000/api/clients/ - clients

http://0.0.0.0:8000/api/messages/ - messages

http://0.0.0.0:8000/api/mailings/ - mailings

http://0.0.0.0:8000/api/mailings/details/ - all available details of mailing status and timestamps

http://0.0.0.0:8000/api/mailings/<pk>/info/ - detailed info of mailing object

---
## Запустите тесты:
```
python manage.py test
```
## Установка с помощью docker-compose
1. Склонируйте репозиторий:
```
git clone https://gitlab.com/eracoding/notification-service.git
```
2. Запустите контейнеры:
```
sudo docker-compose up -d
```
3. Остановить работу контейнеров:
```
sudo docker-compose stop
```
