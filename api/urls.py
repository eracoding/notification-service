from django.urls import path, include
from rest_framework import routers
from api.views import ClientViewSet, MailingViewSet, MessageViewSet

router = routers.DefaultRouter()
router.register(r'clients', ClientViewSet)
router.register(r'messages', MessageViewSet)
router.register(r'mailings', MailingViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
