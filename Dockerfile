FROM python:3.9
ENV DJANGO_ENV dev
ENV PYTHONUNBUFFERED 1
ENV DOCKER_CONTAINER 1

COPY ./requirements.txt /notification/requirements.txt
RUN pip install -r /notification/requirements.txt

COPY . /notification/
WORKDIR /notification/

EXPOSE 8000
EXPOSE 5555
