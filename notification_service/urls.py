from django.contrib import admin
from django.urls import path, include
from .yasg import swaggerurlpatterns

urlpatterns = [
    path("admin/", admin.site.urls),
    path('api/', include('api.urls')),
    path(r'api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('prometheus/', include('django_prometheus.urls')),
]
urlpatterns += swaggerurlpatterns
